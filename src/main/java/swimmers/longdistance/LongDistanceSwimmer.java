package swimmers.longdistance;

/**
 * Created by PK on 10/5/2015.
 */
public class LongDistanceSwimmer {

    private int height;
    private int time;

    public LongDistanceSwimmer(int height) {
        this.height = height;
    }

    public void swim() {
        time = 90+(int) (Math.random()*10);
    }

    public boolean won(LongDistanceSwimmer challenger) {
        return time < challenger.getTime();
    }

    public int getHeight() {
        return height;
    }

    public int getTime() {
        return time;
    }
}
