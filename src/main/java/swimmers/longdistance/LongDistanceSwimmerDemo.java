package swimmers.longdistance;

/**
 * Created by PK on 10/5/2015.
 */
public class LongDistanceSwimmerDemo {

    public static void main(String[] args) {
        LongDistanceSwimmer longDistanceSwimmer1 = new LongDistanceSwimmer(182);
        LongDistanceSwimmer longDistanceSwimmer2 = new LongDistanceSwimmer(180);
        longDistanceSwimmer1.swim();
        longDistanceSwimmer2.swim();

        System.out.println(longDistanceSwimmer1.getTime());
        System.out.println(longDistanceSwimmer2.getTime());
        System.out.println(longDistanceSwimmer1.won(longDistanceSwimmer2));
        System.out.println(longDistanceSwimmer2.won(longDistanceSwimmer1));
    }
}
