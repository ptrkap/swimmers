package swimmers.shortdistance;

/**
 * Created by PK on 10/5/2015.
 */
public class ShortDistanceSwimmer {

    private int height;
    private int time;

    public ShortDistanceSwimmer(int height) {
        this.height = height;
    }

    public void swim() {
        time = 5+(int) (Math.random()*5);
    }

    public boolean won(ShortDistanceSwimmer challenger) {
        return time < challenger.getTime();
    }

    public int getHeight() {
        return height;
    }

    public int getTime() {
        return time;
    }
}
