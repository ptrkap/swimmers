package swimmers.shortdistance;

/**
 * Created by PK on 10/5/2015.
 */
public class ShortDistanceSwimmerDemo {

    public static void main(String[] args) {
        ShortDistanceSwimmer michaelPhelps = new ShortDistanceSwimmer(182);
        ShortDistanceSwimmer pawelKorzeniowski = new ShortDistanceSwimmer(180);
        michaelPhelps.swim();
        pawelKorzeniowski.swim();

        System.out.println(michaelPhelps.getTime());
        System.out.println(pawelKorzeniowski.getTime());
        System.out.println(michaelPhelps.won(pawelKorzeniowski));
        System.out.println(pawelKorzeniowski.won(michaelPhelps));
    }
}
