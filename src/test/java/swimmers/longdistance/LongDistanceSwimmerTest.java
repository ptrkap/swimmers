package swimmers.longdistance;

import org.junit.Test;
import swimmers.shortdistance.ShortDistanceSwimmer;

import static org.junit.Assert.*;

/**
 * Created by PK on 10/5/2015.
 */
public class LongDistanceSwimmerTest {

    LongDistanceSwimmer michaelPhelps = new LongDistanceSwimmer(182);
    LongDistanceSwimmer pawelKorzeniowski = new LongDistanceSwimmer(180);
    LongDistanceSwimmer someChinasDude = new LongDistanceSwimmer(172);

    @Test
    public void swimmerShouldBeHigherOrEqualThan180() {
        final int MIN_HEIGHT = 180;
        assertTrue(michaelPhelps.getHeight() >= MIN_HEIGHT);
        assertTrue(pawelKorzeniowski.getHeight() >= MIN_HEIGHT);
        assertFalse(someChinasDude.getHeight() >= MIN_HEIGHT);
    }

    @Test
    public void swimmingTimeShouldBeBelowOrEqual10(){
        int n = 100;
        for (int i = 0; i < n; i++) {
            michaelPhelps.swim();
            pawelKorzeniowski.swim();
            someChinasDude.swim();
            assertTrue("actual time is: " + michaelPhelps.getTime(),  michaelPhelps.getTime() <= 100);
            assertTrue("actual time is: " + pawelKorzeniowski.getTime(), pawelKorzeniowski.getTime() <= 100);
            assertTrue("actual time is: " + someChinasDude.getTime(), someChinasDude.getTime() <= 100);
        }
    }

    @Test
    public void swimmingTimeShouldBeGreaterOrEqual90(){
        int n = 100;
        for (int i = 0; i < n; i++) {
            michaelPhelps.swim();
            pawelKorzeniowski.swim();
            someChinasDude.swim();
            assertTrue(String.valueOf("actual time is: " + michaelPhelps.getTime()), michaelPhelps.getTime() >= 90);
            assertTrue(String.valueOf("actual time is: " + pawelKorzeniowski.getTime()), pawelKorzeniowski.getTime() >= 90);
            assertTrue(String.valueOf("actual time is: " + someChinasDude.getTime()), someChinasDude.getTime() >= 90);
        }
    }

}