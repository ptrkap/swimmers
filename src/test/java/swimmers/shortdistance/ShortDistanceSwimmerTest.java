package swimmers.shortdistance;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by PK on 10/5/2015.
 */
public class ShortDistanceSwimmerTest {

    ShortDistanceSwimmer michaelPhelps = new ShortDistanceSwimmer(182);
    ShortDistanceSwimmer pawelKorzeniowski = new ShortDistanceSwimmer(180);
    ShortDistanceSwimmer someChinasDude = new ShortDistanceSwimmer(172);

    @Test
    public void swimmerShouldBeHigherOrEqualThan180() {
        final int MIN_HEIGHT = 180;
        assertTrue(michaelPhelps.getHeight() >= MIN_HEIGHT);
        assertTrue(pawelKorzeniowski.getHeight() >= MIN_HEIGHT);
        assertFalse(someChinasDude.getHeight() >= MIN_HEIGHT);
    }

    @Test
    public void swimmingTimeShouldBeBelowOrEqual10(){
        int n = 100;
        for (int i = 0; i < n; i++) {
            michaelPhelps.swim();
            pawelKorzeniowski.swim();
            someChinasDude.swim();
            assertTrue("actual time is: " + michaelPhelps.getTime(),  michaelPhelps.getTime() <= 10);
            assertTrue("actual time is: " + pawelKorzeniowski.getTime(), pawelKorzeniowski.getTime() <= 10);
            assertTrue("actual time is: " + someChinasDude.getTime(), someChinasDude.getTime() <= 10);
        }
    }

    @Test
    public void swimmingTimeShouldBeGreaterOrEqual5(){
        int n = 100;
        for (int i = 0; i < n; i++) {
            michaelPhelps.swim();
            pawelKorzeniowski.swim();
            someChinasDude.swim();
            assertTrue(String.valueOf("actual time is: " + michaelPhelps.getTime()), michaelPhelps.getTime() >= 5);
            assertTrue(String.valueOf("actual time is: " + pawelKorzeniowski.getTime()), pawelKorzeniowski.getTime() >= 5);
            assertTrue(String.valueOf("actual time is: " + someChinasDude.getTime()), someChinasDude.getTime() >= 5);
        }
    }

}